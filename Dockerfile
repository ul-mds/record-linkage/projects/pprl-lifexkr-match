FROM python:3.10.8

WORKDIR /app
COPY . .

RUN pip install poetry -q && poetry install -q

ENTRYPOINT ["poetry", "run", "pprl_match"]

CMD ["-h"]