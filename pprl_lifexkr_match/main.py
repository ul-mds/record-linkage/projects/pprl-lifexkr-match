import argparse
import csv
import logging
import os
from typing import TextIO, NamedTuple

import requests


class BitVector(NamedTuple):
    id: str
    value: str


class MatchedBitVector(NamedTuple):
    domain: BitVector
    range: BitVector
    similarity: float


logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)


def match(
        # args
        domain_vectors: list[BitVector],
        range_vectors: list[BitVector],
        similarity_threshold: float,
        similarity_measure: str,
        match_url: str
) -> list[MatchedBitVector]:
    logging.info("Matching bit vectors ...")

    r = requests.post(match_url, json={
        "config": {
            "measure": similarity_measure,
            "threshold": similarity_threshold
        },
        "domain": [
            {
                "id": d.id,
                "value": d.value
            } for d in domain_vectors
        ],
        "range": [
            {
                "id": r.id,
                "value": r.value
            } for r in range_vectors
        ]
    })

    if r.status_code != 200:
        logging.error("Unexpected status code: %d", r.status_code)
        logging.error("Response: %s", r.text)
        exit(1)

    matches = r.json()["matches"]

    return [
        MatchedBitVector(
            domain=BitVector(
                id=m["domain"]["id"],
                value=m["domain"]["value"]
            ),
            range=BitVector(
                id=m["range"]["id"],
                value=m["range"]["value"]
            ),
            similarity=m["similarity"]
        ) for m in matches
    ]


def run_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument("domain", type=argparse.FileType(mode="r", encoding="utf-8"))
    parser.add_argument("range", type=argparse.FileType(mode="r", encoding="utf-8"))
    parser.add_argument("output", type=argparse.FileType(mode="w", encoding="utf-8"))

    parser.add_argument("--domain-id", type=str, default="id")
    parser.add_argument("--range-id", type=str, default="id")

    parser.add_argument("-s", "--similarity", type=str, default="jaccard", choices=["dice", "cosine", "jaccard"])
    parser.add_argument("-t", "--threshold", type=float, default=0.9)

    parser.add_argument("--match-url", type=str, default="http://localhost:8080/match")
    parser.add_argument("--chown", type=str, default=None)

    args = parser.parse_args()

    domain_file: TextIO = args.domain
    range_file: TextIO = args.range

    def read_vector_csv(in_file: TextIO, id_col: str) -> list[BitVector]:
        in_csv = csv.DictReader(in_file)
        return [BitVector(row[id_col], row["vector"]) for row in in_csv]

    domain_vec = read_vector_csv(domain_file, args.domain_id)
    range_vec = read_vector_csv(range_file, args.range_id)

    domain_file.close()
    range_file.close()

    matches = match(domain_vec, range_vec, args.threshold, args.similarity, args.match_url)

    output_file: TextIO = args.output

    out_csv = csv.DictWriter(output_file, fieldnames=["domain_id", "range_id", "similarity"])
    out_csv.writeheader()

    out_csv.writerows(
        {
            "domain_id": m.domain.id,
            "range_id": m.range.id,
            "similarity": m.similarity
        } for m in matches
    )

    if args.chown is not None:
        uid_str, gid_str = args.chown.split(":")
        uid, gid = int(uid_str), int(gid_str)

        os.fchown(output_file.fileno(), uid, gid)

    output_file.close()


if __name__ == "__main__":
    run_cli()
