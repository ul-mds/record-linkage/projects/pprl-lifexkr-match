# LIFExKR entity matching

This project contains scripts and service definitions to perform entity matching for privacy-preserving record linkage.

## How to use

You need to [install Docker and Docker Compose](https://docs.docker.com/engine/install/).
You will need two CSV files with data that you want to match.
The CSV files may look as follows.

| **id** | **vector**                       |
|--------|----------------------------------|
| 001    | CSnqzFJPJlJ6FZPqWDuPH7idECLrr... |
| 002    | FAiLpAIyPh1Knk6PJv_nW85xoPeqh... |
| 003    | CkkiQRK7Suri02IPJn_aM9xlEaF-o... |
| ...    | ...                              |

Copy them to the [data directory](./data).
Rename one to `domain.csv` and one to `range.csv`.

Finally, edit the [Docker Compose file](./docker-compose.yml).
Go to `command` and scroll to the end.
Run `id -a` in a console to determine your `uid` and `gid`.
Edit the `command` and replace the `--chown` argument with your user's `uid` and `gid`.

Run `docker compose up -d`.
A container for the script should be built and executed.
The execution of the script shouldn't take too long.
You can check whether the script container has completed by running `docker compose ps`.
The script container must complete with the exit code 0.
By the end, you will be able to find an output file in the [data directory](./data/).

## License

MIT.